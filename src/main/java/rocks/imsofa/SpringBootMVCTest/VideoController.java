package rocks.imsofa.SpringBootMVCTest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class VideoController {
    String[] videourl = { "GKE23X-1HVE&t=21s", "G52dUQLxPzg", "9do1Uw4w-IU" };

    @GetMapping("/video")
    public String indexAction(Model model, @RequestParam int num) {
        num++;
        model.addAttribute("id", videourl[num]);
        model.addAttribute("num", num);
        if (num < videourl.length - 1) {
            model.addAttribute("nextURL", "/video?num=" + num);
        } else {
            model.addAttribute("nextURL", "/");
        }
        return "video";
    }
}