package rocks.imsofa.SpringBootMVCTest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @GetMapping("/")
    public String indexAction(Model model){ 
        model.addAttribute("name", "lendle");
        return "index";
    }
}